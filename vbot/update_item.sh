#!/bin/bash

SCRIPT_DIR='/home/django/dev_oblakomody_ru/xshop2/'
source /home/django/dev_oblakomody_ru/env/bin/activate

date &&
cd $SCRIPT_DIR &&
python manage.py ad_import &&
date &&
python manage.py change_new_item &&
date &&
service uwsgi restart &&
service nginx restart 

# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from vbot.semantic import cloning_subtext, cloning_title


class Command(BaseCommand):
    help = 'Semantic text generator'

    def handle(self, *args, **options):
        print u"cloning_subtext()"
        cloning_subtext()
        print u"cloning_title()"
        cloning_title()
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from vbot.adimport import start


class Command(BaseCommand):
    help = 'import items from CSV to db'

    def handle(self, *args, **options):
        start(update=True)
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from vbot.change_new_item import start


class Command(BaseCommand):
    help = 'processing items in database'

    def handle(self, *args, **options):
        start()
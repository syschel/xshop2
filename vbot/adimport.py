# -*- coding: utf-8 -*-
import sys
import urllib
import time
import math
import csv
from django.utils import timezone
from datetime import datetime, timedelta
from django.conf import settings
from django.db import connection
from catalog.models import Category, Item
cursor = connection.cursor()


def start(update=False):
    file_url = "%s/files/adimport_items.csv" % settings.BASE_DIR
    category = Category.objects.filter(show=True).only('pk', 'csv_adimport')
    for cat in category:
        csv_file = cat.csv_adimport
        csv_file = '%s&fields_name=true' % csv_file
        if update and cat.last_import:
            csv_file = csv_file.replace('&last_import=', '&last_import=%s' % datetime.strftime(cat.last_import, '%Y.%m.%d.%H.%m'))

        test_url = 'http://adimport.ru/export/?id=14733&last_import=&user=31&format=csv&fields_name=true'

        # print u'-= download(%s):' % cat.pk, csv_file
        t0 = time.clock()
        urllib.urlretrieve(csv_file, file_url)
        # print 'download file:', time.clock() - t0
        t1 = time.clock()
        file_to_mysql(file_url)
        # print 'file_to_mysql(): ', time.clock() - t1

        cat.last_import = timezone.now()
        cat.save()

        t2 = time.clock()
        temp_to_project(category=cat.pk)
        # print 'temp_to_project(%s): ' % cat.pk, time.clock() - t2
        t3 = time.clock()
        delete_old_items()
        # print 'delete_old_items(): ', time.clock() - t3

    return True

"""
Удаляем лишние столбцы
sudo cut -d"|" -f1,5,8,14,15,16,20,65,27,30,33,34,43,44,48,49 ./1.csv > 2.csv

Загружаем кашерный файл
LOAD DATA LOCAL INFILE '/srv/django/cpa/cms_cpa/files/adimport_items.csv' INTO TABLE adimport_tmp CHARACTER SET utf8 FIELDS TERMINATED BY '|' ENCLOSED BY "'" LINES TERMINATED BY '\n' IGNORE 1 LINES (id_adimport,article,available,currencyId,delivery,description,id,name,oldprice,param,picture,price,url,vendor,advcampaign_id,advcampaign_name);

Загружаем только нужные поля (!!!)
LOAD DATA LOCAL INFILE '/srv/django/cpa/cms_cpa/files/adimport_items.csv' INTO TABLE adimport_tmp CHARACTER SET utf8 FIELDS TERMINATED BY '|' ENCLOSED BY "'" LINES TERMINATED BY '\n' IGNORE 1 LINES (id_adimport,@ISBN,@adult,@age,article,@attrs,@author,available,@barcode,@binding,@brand,@categoryId,@country_of_origin,currencyId,delivery,description,@downloadable,@format,@gender,id,@local_delivery_cost,@manufacturer_warranty,@market_category,@model,@modified_time,name,oldprice,@orderingTime,@page_extent,param,@performed_by,@pickup,picture,price,@publisher,@sales_notes,@series,@store,@syns,@topseller,@type,@typePrefix,url,vendor,@vendorCode,@weight,@year,advcampaign_id,advcampaign_name,@deeplink);

Все поля
LOAD DATA LOCAL INFILE '/srv/django/cpa/cms_cpa/files/adimport_items.csv' INTO TABLE adimport_tmp CHARACTER SET utf8 FIELDS TERMINATED BY '|' ENCLOSED BY "'" LINES TERMINATED BY '\n' IGNORE 1 LINES (id_adimport,ISBN,adult,age,article,attrs,author,available,barcode,binding,brand,categoryId,country_of_origin,currencyId,delivery,description,downloadable,format,gender,id,local_delivery_cost,manufacturer_warranty,market_category,model,modified_time,name,oldprice,orderingTime,page_extent,param,performed_by,pickup,picture,price,publisher,sales_notes,series,store,syns,topseller,type,typePrefix,url,vendor,vendorCode,weight,year,advcampaign_id,advcampaign_name,deeplink);
"""


def file_to_mysql(file):
    # Загружаем файл во временную БД
    # print 'upload in database:', file

    sql = u"LOAD DATA LOCAL INFILE '%s' INTO TABLE adimport_tmp CHARACTER SET utf8 FIELDS TERMINATED BY '|'" \
          u" ENCLOSED BY \"'\" LINES TERMINATED BY '\n' IGNORE 1 LINES (id_adimport,@ISBN,@adult,@age,article," \
          u"@attrs,@author,available,@barcode,@binding,@brand,@categoryId,@country_of_origin,currencyId,delivery," \
          u"description,@downloadable,@format,@gender,id,@local_delivery_cost,@manufacturer_warranty,@market_category," \
          u"@model,@modified_time,name,oldprice,@orderingTime,@page_extent,param,@performed_by,@pickup,picture,price," \
          u"@publisher,@sales_notes,@series,@store,@syns,@topseller,@type,@typePrefix,url,vendor,@vendorCode,@weight," \
          u"@year,advcampaign_id,advcampaign_name,@deeplink);" % file
    cursor.execute(sql)

    update_sql = u"UPDATE `adimport_tmp` SET `oldprice` = '0' WHERE `oldprice` = '';"
    cursor.execute(update_sql)
    return True


def temp_to_project(category=None):
    # Перемещаем данные из временной таблицы в основную
    today = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    # Переносим из временной БД данные в рабочую
    sql = u"INSERT INTO `catalog_item` (`id`, `create`, `update`, `show`, `url`, `subtext`, `name`, `price`," \
          u" `oldprice`, `description`, `id_adimport`, `picture`, `available`, `currency`, `delivery`, `admitad_id`," \
          u" `model`, `ref_url`, `article`, `vendor_name`, `advcampaign_id`, `advcampaign_name`, `param`," \
          u" `category_id`, `offer_id`, `vendor_id`, `new_item`) " \
          u"SELECT NULL, '%s', '%s', '0', NULL, NULL, `name`, `price`," \
          u" `oldprice`, NULL, `id_adimport`, `picture`, `available`, `currencyId`, `delivery`, `id`," \
          u" NULL, `url`, `article`, `vendor`, `advcampaign_id`, `advcampaign_name`, `param`," \
          u" '%s', NULL, NULL, '1' " \
          u"FROM `adimport_tmp` WHERE `adimport_tmp`.`url` IS NOT NULL" \
          u" ON DUPLICATE KEY UPDATE `catalog_item`.`price` = `adimport_tmp`.`price`," \
          u" `catalog_item`.`oldprice` = `adimport_tmp`.`oldprice`" \
          u";" % (today, today, category)
    cursor.execute(sql)
    # Удаляем товары "не на удаление" из временной БД
    delete_sql = u"DELETE FROM `adimport_tmp` WHERE `url` IS NOT NULL;"
    cursor.execute(delete_sql)

    return True


def delete_old_items():
    # Отключаем товары пришедшие на удаление
    today = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    show_sql = u"UPDATE `catalog_item` SET `show`='0', `delete`='%s' WHERE `id_adimport`IN (SELECT `id_adimport`" \
               u" FROM `adimport_tmp` WHERE `url` IS NULL);" % today
    cursor.execute(show_sql)
    # Удаляем товары "на удаление" из временной БД
    delete_show_sql = u"DELETE FROM `adimport_tmp` WHERE `url` IS NULL;"
    cursor.execute(delete_show_sql)
    convert_null_param_sql = u"UPDATE `catalog_item` SET `param` = NULL WHERE `param` = '' OR `param` = '0';"
    cursor.execute(convert_null_param_sql)
    return True

#-*- coding:utf-8 -*-
from django.contrib import admin
from content.models import Pages, UploadFiles, SeoDefault, BaseSettings, FooterMenu, FooterMenuLink


class PagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'show', 'create', 'update')
    list_display_links = ('name',)
    list_filter = ('show',)
    search_fields = ['name']

    fieldsets = (
        (u"SEO", {'fields': ('title', 'keywords', 'description', 'url')}),
        (u"Общее", {'fields': ('name', 'images', 'min_text', 'text')}),
        (u"Настройки", {'fields': ('show',)}),
    )


class BaseSettingsAdmin(admin.ModelAdmin):
    list_display = ('template_seo',)
    list_display_links = ('template_seo',)
    fieldsets = (
        (None, {'fields': ('template_seo',)}),
        (u'Информация', {'fields': ('meta', 'footer', 'robots',)}),
        (u'SEO', {'fields': ('name', 'about', 'title', 'keywords', 'description',)}),
    )


class UploadFilesAdmin(admin.ModelAdmin):
    list_display = ('name', 'file', 'created',)
    list_display_links = ('name', 'file',)
    ordering = ('-created',)


class SeoDefaultAdmin(admin.ModelAdmin):
    list_display = ('template_seo', 'text_seo', 'created',)
    list_display_links = ('template_seo',)
    ordering = ('-created',)


class FooterMenuLinkInline(admin.TabularInline):
    model = FooterMenuLink
    extra = 1


class FooterMenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'level',)
    list_display_links = ('name', 'level',)
    inlines = [FooterMenuLinkInline]


admin.site.register(Pages, PagesAdmin)
admin.site.register(SeoDefault, SeoDefaultAdmin)
admin.site.register(UploadFiles, UploadFilesAdmin)
admin.site.register(FooterMenu, FooterMenuAdmin)
admin.site.register(BaseSettings, BaseSettingsAdmin)

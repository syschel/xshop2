#-*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from PIL import Image
from tinymce.models import HTMLField
import re
import unidecode
from kernel.files import get_file_path


class Pages(models.Model):
    """
    Статичные страницы
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    images          = models.ImageField(verbose_name=u'Изображение', upload_to=get_file_path, blank=True, null=True)
    min_text        = models.TextField(verbose_name=u'Краткое описание', null=True, blank=True)
    text            = HTMLField(verbose_name=u'Подробное описание', null=True, blank=True)

    url             = models.SlugField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    title           = models.CharField(verbose_name=u'title', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'keywords', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'description', max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('content:page', args=[self.url, self.pk])

    def save(self, size=(100, 100), *args, **kwargs):
        # Если нет титлы, воруем из названия категории
        if not self.title:
            self.title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(Pages, self).save(*args, **kwargs)

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = u'Pages'
        verbose_name_plural = u'Page'
        ordering = ('-create',)


class UploadFiles(models.Model):
    """ Модель загрузки файлов """

    name            = models.CharField(verbose_name=u'Название файла', max_length=255)
    file            = models.FileField(verbose_name=u'Файл', upload_to=get_file_path, blank=True, null=True)
    created         = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Загрузка файлов'
        verbose_name_plural = u'Загрузка файлов'


class SeoDefault(models.Model):
    """ Шаблоны СЕО """

    TEMPL0, TEMPL1, TEMPL2, TEMPL3, TEMPL4, TEMPL5, TEMPL6, TEMPL7, TEMPL8 = range(0, 9)
    TEMPLATE = (
        (TEMPL0, u'title_category'),
        (TEMPL1, u'keywords_category'),
        (TEMPL2, u'description_category'),
        (TEMPL3, u'title_item'),
        (TEMPL4, u'keywords_item'),
        (TEMPL5, u'description_item'),
        (TEMPL6, u'title_shops_list'),
        (TEMPL7, u'keywords_shops_list'),
        (TEMPL8, u'description_shops_list'),
    )

    text_seo        = models.CharField(verbose_name=u'Шаблон', max_length=255,
                                       help_text=u"__names__ - название объекта(не краткое),<br>"
                                                 u"__prices__ - цена товара,<br>"
                                                 u"__offers__ - оффер товара,<br>"
                                                 u"__categorys__ - категория товара")
    template_seo    = models.IntegerField(verbose_name=u'#Тип шаблона', default=TEMPL0, choices=TEMPLATE, unique=True)
    created         = models.DateTimeField(verbose_name=u'Дата создания', auto_now_add=True,)

    def __unicode__(self):
        return u"%s" % self.get_template_seo_display()

    class Meta:
        verbose_name = u'Шаблоны СЕО'
        verbose_name_plural = u'Шаблоны СЕО'


class BaseSettings(models.Model):
    """ Профиль домена """
    DEFAULT = ((0, u'base'),)
    template_seo    = models.IntegerField(verbose_name=u'unique', default=0, choices=DEFAULT, unique=True)

    meta            = models.TextField(verbose_name=u'Настройки meta-тегов', null=True, blank=True)
    robots          = models.TextField(verbose_name=u'Настройки robots.txt', null=True, blank=True)
    footer          = models.TextField(verbose_name=u'Вывод кода перед </body>', null=True, blank=True)

    name            = models.CharField(verbose_name=u'title', max_length=255, blank=True, null=True)
    title           = models.CharField(verbose_name=u'title', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'keywords', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'description', max_length=255, blank=True, null=True)
    about           = models.TextField(verbose_name=u'Описание главной', null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.template_seo

    class Meta:
        verbose_name = u'Base Settings'
        verbose_name_plural = u'Base Settings'


class FooterMenu(models.Model):
    """ Профиль домена """

    LEVEL0, LEVEL1, LEVEL2, LEVEL3 = range(0, 4)
    LEVELS = (
        (LEVEL0, u'left 1'),
        (LEVEL1, u'left 2'),
        (LEVEL2, u'left 3'),
        (LEVEL3, u'left 4'),

    )

    level = models.IntegerField(verbose_name=u'Колонка', default=LEVEL0, choices=LEVELS, unique=True)
    name = models.CharField(verbose_name=u'Название', max_length=255)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Footer Menu'
        verbose_name_plural = u'Footer Menu'


class FooterMenuLink(models.Model):
    subject = models.ForeignKey(FooterMenu, verbose_name=u'Колонка', related_name='footer_menu')
    name = models.CharField(verbose_name=u'Название', max_length=255)
    url = models.CharField(verbose_name=u'Link', max_length=255)
    sort = models.IntegerField(verbose_name=u"#Порядок сортировки", default=0, blank=True,)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        ordering = ('sort',)
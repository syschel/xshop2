# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models
import kernel.files


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('template_seo', models.IntegerField(default=0, unique=True, verbose_name='unique', choices=[(0, 'base')])),
                ('meta', models.TextField(null=True, verbose_name='\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 meta-\u0442\u0435\u0433\u043e\u0432', blank=True)),
                ('robots', models.TextField(null=True, verbose_name='\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 robots.txt', blank=True)),
                ('footer', models.TextField(null=True, verbose_name='\u0412\u044b\u0432\u043e\u0434 \u043a\u043e\u0434\u0430 \u043f\u0435\u0440\u0435\u0434 </body>', blank=True)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='title', blank=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='title', blank=True)),
                ('keywords', models.CharField(max_length=255, null=True, verbose_name='keywords', blank=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='description', blank=True)),
                ('about', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0433\u043b\u0430\u0432\u043d\u043e\u0439', blank=True)),
            ],
            options={
                'verbose_name': 'Base Settings',
                'verbose_name_plural': 'Base Settings',
            },
        ),
        migrations.CreateModel(
            name='FooterMenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField(default=0, unique=True, verbose_name='\u041a\u043e\u043b\u043e\u043d\u043a\u0430', choices=[(0, 'left 1'), (1, 'left 2'), (2, 'left 3'), (3, 'left 4')])),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': 'Footer Menu',
                'verbose_name_plural': 'Footer Menu',
            },
        ),
        migrations.CreateModel(
            name='FooterMenuLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('url', models.CharField(max_length=255, verbose_name='Link')),
                ('sort', models.IntegerField(default=0, verbose_name='#\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', blank=True)),
                ('subject', models.ForeignKey(related_name='footer_menu', verbose_name='\u041a\u043e\u043b\u043e\u043d\u043a\u0430', to='content.FooterMenu')),
            ],
            options={
                'ordering': ('sort',),
            },
        ),
        migrations.CreateModel(
            name='Pages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('images', models.ImageField(upload_to=kernel.files.get_file_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('min_text', models.TextField(null=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('text', tinymce.models.HTMLField(null=True, verbose_name='\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('url', models.SlugField(max_length=255, null=True, verbose_name='\u0427\u041f\u0423 URL', blank=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='title', blank=True)),
                ('keywords', models.CharField(max_length=255, null=True, verbose_name='keywords', blank=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='description', blank=True)),
            ],
            options={
                'ordering': ('-create',),
                'verbose_name': 'Pages',
                'verbose_name_plural': 'Page',
            },
        ),
        migrations.CreateModel(
            name='SeoDefault',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text_seo', models.CharField(help_text='__names__ - \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043e\u0431\u044a\u0435\u043a\u0442\u0430(\u043d\u0435 \u043a\u0440\u0430\u0442\u043a\u043e\u0435),<br>__prices__ - \u0446\u0435\u043d\u0430 \u0442\u043e\u0432\u0430\u0440\u0430,<br>__offers__ - \u043e\u0444\u0444\u0435\u0440 \u0442\u043e\u0432\u0430\u0440\u0430,<br>__categorys__ - \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0442\u043e\u0432\u0430\u0440\u0430', max_length=255, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d')),
                ('template_seo', models.IntegerField(default=0, unique=True, verbose_name='#\u0422\u0438\u043f \u0448\u0430\u0431\u043b\u043e\u043d\u0430', choices=[(0, 'title_category'), (1, 'keywords_category'), (2, 'description_category'), (3, 'title_item'), (4, 'keywords_item'), (5, 'description_item'), (6, 'title_shops_list'), (7, 'keywords_shops_list'), (8, 'description_shops_list')])),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u0428\u0430\u0431\u043b\u043e\u043d\u044b \u0421\u0415\u041e',
                'verbose_name_plural': '\u0428\u0430\u0431\u043b\u043e\u043d\u044b \u0421\u0415\u041e',
            },
        ),
        migrations.CreateModel(
            name='UploadFiles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0444\u0430\u0439\u043b\u0430')),
                ('file', models.FileField(upload_to=kernel.files.get_file_path, null=True, verbose_name='\u0424\u0430\u0439\u043b', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0444\u0430\u0439\u043b\u043e\u0432',
                'verbose_name_plural': '\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0444\u0430\u0439\u043b\u043e\u0432',
            },
        ),
    ]

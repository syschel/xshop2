# -*- coding:utf-8 -*-
from django.conf.urls import patterns, url
from content.views import PageDetailView, PagesListView


urlpatterns = patterns('content.views',
    url(r'^$', PagesListView.as_view(), name='pagelist'),
    url(r'^(?P<slug>\w+)_(?P<pk>\d+)/$', PageDetailView.as_view(), name='page'),
)
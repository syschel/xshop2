#-*- coding:utf-8 -*-
import copy
from django.views.generic import DetailView, ListView, View
from django.views.generic.base import RedirectView, TemplateView
from django.shortcuts import render_to_response
from catalog.tools import gen_page_list
from content.models import Pages, BaseSettings


class PagesListView(ListView):
    """
    Выводим список FAQ
    """
    model = Pages
    context_object_name = 'pages'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(PagesListView, self).get_context_data(**kwargs)
        page_number = self.kwargs.get(self.page_kwarg) or self.request.GET.get(self.page_kwarg) or 1
        page_count = context['paginator'].num_pages
        context['page_lists'] = gen_page_list(int(page_number), int(page_count))
        return context


class PageDetailView(DetailView):
    """
    Выводим статичную страницу с текстом
    """
    model = Pages
    context_object_name = 'page'
    slug_field = 'url'
    query_pk_and_slug = True


def pages_index():
    pages = Pages.objects.filter(show=True)[:4]
    slid_pages = []
    tmp_pages = []
    num = 0
    num2 = 0
    for page in pages:
        num2 += 1
        if num == 2:
            num = 0
            slid_pages.append(copy.copy(tmp_pages))
            del tmp_pages[:]
        if num2 == 4:
            tmp_pages.append(page)
            slid_pages.append(copy.copy(tmp_pages))
        tmp_pages.append(page)
        num += 1
    return slid_pages


class Robotstxt(TemplateView):
    content_type = 'text/plain; charset=utf-8'
    template_name = 'robots.txt'

    def get_context_data(self, **kwargs):
        context = super(Robotstxt, self).get_context_data(**kwargs)
        robots = BaseSettings.objects.filter(template_seo=0).values('robots')
        context['content'] = robots[0] if robots else None
        return context
# -*- coding: utf-8 -*-
import re, unidecode
from django.db import models
from django.core.urlresolvers import reverse
from tinymce.models import HTMLField
from kernel.files import get_file_path


class Category(models.Model):
    """
    Список категорий
    """
    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)
    parent          = models.ForeignKey('self', verbose_name=u"Родительская категория", blank=True, null=True,
                                        help_text=u"Пустое значение, если категория первого уровня",
                                        related_name='category_parent')
    sort            = models.IntegerField(verbose_name=u"#Порядок сортировки", default=100, blank=True,
                                          help_text=u"Возможны отрицательные значения")

    left            = models.IntegerField(blank=True, null=True, default=0)
    right           = models.IntegerField(blank=True, null=True, default=0)
    level           = models.IntegerField(blank=True, null=True, default=0)
    home            = models.BooleanField(verbose_name=u'На главной', default=False)

    csv_adimport    = models.CharField(verbose_name=u'CSV URL', max_length=255, null=True, blank=False)
    update_img      = models.BooleanField(verbose_name=u'Upd img', default=False, help_text=u"Если стоит галочка, значит при обновлении товаров обновить картинки")

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    images          = models.ImageField(verbose_name=u'Изображение', upload_to=get_file_path, blank=True, null=True)
    h1              = models.CharField(verbose_name=u'Заголовок страницы H1', max_length=255, blank=True, null=True)
    title           = models.CharField(verbose_name=u'title', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'keywords', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'description', max_length=255, blank=True, null=True)
    text            = HTMLField(verbose_name=u'Описание', null=True, blank=True)

    last_import     = models.DateTimeField(verbose_name=u"last import", blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('category', args=[self.url, self.pk])

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.strip().replace(" ", "_")))
        if not self.title:
            self.title = self.name.strip()
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name.strip()
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name.strip()
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'Категории'
        verbose_name_plural = u'01. Категории'
        ordering = ('-sort', 'name')


class Item(models.Model):
    """
    Товары
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=False, db_index=True)

    category        = models.ForeignKey(Category, verbose_name=u'Категория', related_name='item_category')
    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    subtext         = models.TextField(verbose_name=u'Доп описание', null=True, blank=True, help_text=u"Синонимизированный текст")

    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=False)
    price           = models.IntegerField(verbose_name=u"Цена", default=0, blank=True)
    oldprice        = models.IntegerField(verbose_name=u"Старая Цена", default=0, blank=True)
    description     = models.TextField(verbose_name=u'Описание', null=True, blank=True)
    id_adimport     = models.CharField(verbose_name=u'id_adimport', max_length=255, null=True, blank=True, unique=True)

    picture         = models.TextField(verbose_name=u'Картинки', null=True, blank=True)
    available       = models.CharField(verbose_name=u'Наличие/Заказ', max_length=255, null=True, blank=True, help_text=u"true: В наличии, false: Под заказ")
    currency        = models.CharField(verbose_name=u'Валюта', max_length=255, null=True, blank=True)
    delivery        = models.CharField(verbose_name=u'Доставка', max_length=255, null=True, blank=True, help_text=u"true: Есть, false: Нет")
    admitad_id      = models.CharField(verbose_name=u'ID в адмитаде', max_length=255, null=True, blank=True)
    model           = models.CharField(verbose_name=u'Модель', max_length=255, null=True, blank=True)
    ref_url         = models.CharField(verbose_name=u'Рефералка', max_length=255, null=True, blank=True)
    article         = models.CharField(verbose_name=u'Артикул товара', max_length=255, null=True, blank=True)

    vendor_name     = models.CharField(verbose_name=u'Бренд', max_length=255, null=True, blank=True, db_index=True)
    advcampaign_id  = models.CharField(verbose_name=u'Идентификатор оффера у admitad.com', max_length=255, null=True, blank=True, db_index=True)
    advcampaign_name = models.CharField(verbose_name=u'Название оффера у admitad.com', max_length=255, null=True, blank=True, db_index=True)
    param           = models.TextField(verbose_name=u'Param', null=True, blank=True)

    offer           = models.ForeignKey('ItemOffer', verbose_name=u"Offer", null=True, blank=True, related_name='offer_item')
    vendor          = models.ForeignKey('ItemVendor', verbose_name=u"Бренд", null=True, blank=True, related_name='vendor_item')

    title           = models.CharField(verbose_name=u'SEO Title', max_length=255, blank=True, null=True,
                                       help_text=u"Заполняется роботом по списку шаблонов")

    new_item        = models.BooleanField(verbose_name=u'Новый', default=True, editable=False)
    delete          = models.DateTimeField(verbose_name=u'Удалён', blank=True, null=True, editable=False)

    def __unicode__(self):
        return u"%s" % self.name

    def image(self):
        if self.picture.split(' '):
            return self.picture.split(' ')[0]
        else:
            return False

    def images(self):
        return self.picture.split(' ')

    def get_absolute_url(self):
        return reverse('item', args=[self.url, self.pk])

    def get_goto_url(self):
        return reverse('redirect_item', args=[self.pk])

    def percent(self):
        return 100 - ((self.price * 100) / self.oldprice)

    class Meta:
        verbose_name = u'Товары'
        verbose_name_plural = u'02. Товары'
        ordering = ['create']


class ItemOffer(models.Model):
    """
    Оффер
    """
    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)
    name            = models.CharField(verbose_name=u'Название', max_length=255, null=True, blank=True, unique=True)
    code            = models.CharField(verbose_name=u'Код офера', max_length=255, null=True, blank=True)
    images          = models.ImageField(verbose_name=u'Изображение офера', upload_to=get_file_path, blank=True, null=True)

    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)
    min_text        = HTMLField(verbose_name=u'Краткий текст', null=True, blank=True)
    max_text        = HTMLField(verbose_name=u'Описание', null=True, blank=True)
    title           = models.CharField(verbose_name=u'СЕО-Заголовок окна', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'СЕО-Ключевые', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'СЕО-Описание', max_length=255, blank=True, null=True)

    delivery        = models.BooleanField(verbose_name=u'Доставка', default=False)
    payment         = HTMLField(verbose_name=u'Способы оплаты', null=True, blank=True)
    return_policy   = HTMLField(verbose_name=u'Условие доставки', null=True, blank=True)
    guarantee       = HTMLField(verbose_name=u'Гарантии', null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.name

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.strip().replace(" ", "_")))
        super(ItemOffer, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'Оферы'
        verbose_name_plural = u'03. Оферы'


class ItemVendor(models.Model):
    """
    Товары производитель (бренд)
    """

    create          = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    update          = models.DateTimeField(verbose_name=u"Обновлено", auto_now=True)
    show            = models.BooleanField(verbose_name=u'Отображать', default=True)

    name            = models.CharField(verbose_name=u'Бренд', max_length=255, null=True, blank=True, unique=True)
    code            = models.CharField(verbose_name=u'Код бренда', max_length=255, null=True, blank=True)
    url             = models.CharField(verbose_name=u'ЧПУ URL', max_length=255, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.strip().replace(" ", "_")))
        super(ItemVendor, self).save(*args, **kwargs)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Бренды'
        verbose_name_plural = u'04. Бренды'


class ItemParams(models.Model):
    """
    Характеристики товара
    """
    item           = models.ForeignKey('Item', verbose_name=u"Товар", related_name='param_item')
    attr           = models.ForeignKey('ItemParamsName', verbose_name=u'Название характеристики', related_name='param_name')
    value          = models.CharField(max_length=20000, verbose_name=u'Значение', null=True, blank=True)
    sub_value      = models.CharField(max_length=500, verbose_name=u'Unit значение', help_text=u"Величина измерения: КГ, СМ", null=True, blank=True)

    def __unicode__(self):
        return u'%s - %s' % (self.attr, self.value)

    class Meta:
        verbose_name = u'Характеристики'
        verbose_name_plural = u'05. Характеристики'


class ItemParamsName(models.Model):
    """
    Список характеристик
    """
    name = models.CharField(max_length=255, verbose_name=u'Название хар-тики', help_text=u"Цвет, размер, вес, ...", unique=True)
    show = models.BooleanField(verbose_name=u'Отображать', default=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = u'Названия характеристик'
        verbose_name_plural = u'06. Названия характеристик'
        ordering = ['name']


class FilterSetting(models.Model):
    """
    Настройки фильтров
    """
    name        = models.CharField(max_length=300, verbose_name=u'Отображаемое имя', help_text=u"Выводится в категории")
    name_show   = models.CharField(max_length=300, verbose_name=u'Имя в админке', help_text=u"Выводится только в админке")
    show        = models.BooleanField(verbose_name=u'Отображать', default=True)
    categoryes  = models.ManyToManyField('Category', verbose_name=u'Категория', help_text=u"Список категорий где можно вывести этот фильтр")
    param_name  = models.ForeignKey('ItemParamsName', verbose_name=u'Характеристика из CSV')

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = u'Настройки фильтров'
        verbose_name_plural = u'07. Настройки фильтров'


class FilterSettingValues(models.Model):
    """
    Настройки фильтров список значений
    """
    param_name  = models.ForeignKey('FilterSetting', verbose_name=u'Настройки фильтра', related_name='filters_settings')
    value       = models.CharField(max_length=300, verbose_name=u'Значение')

    def __unicode__(self):
        return u"%s" % self.value

    class Meta:
        verbose_name = u'Значение фильтра'
        verbose_name_plural = u'Значения фильтра'


class SemanticText(models.Model):
    """
    Списки описаний товаров от синонимайзера
    """

    category        = models.OneToOneField('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список описаний", null=True, blank=True,
                                       help_text=u"Разделитель с новой строки.<br>")

    def __unicode__(self):
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки описаний'
        verbose_name_plural = u'08. Списоки описаний'
        ordering = ['category_id']


class SemanticReviews(models.Model):
    """
    Списки отзывов от синонимайзера
    """

    category        = models.OneToOneField('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список отзывов", null=True, blank=True,
                                       help_text=u'Разделитель с новой строки.')

    def __unicode__(self):
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки отзывов'
        verbose_name_plural = u'09. Списоки отзывов'
        ordering = ['category_id']


class SemanticTitle(models.Model):
    """
    Списки заголовков от синонимайзера
    """

    category        = models.OneToOneField('Category', verbose_name=u'Категория')
    value           = models.TextField(verbose_name=u"Список отзывов", null=True, blank=True,
                                       help_text=u"Разделитель с новой строки.<br>"
                                                 u"__names__ - название объекта(не краткое),<br>"
                                                 u"__prices__ - цена товара,<br>"
                                                 u"__offers__ - оффер товара,<br>"
                                                 u"__categorys__ - категория товара")

    def __unicode__(self):
        return u"%s" % self.category.name

    class Meta:
        verbose_name = u'Списоки Title'
        verbose_name_plural = u'10. Списоки Title'
        ordering = ['category_id']
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import kernel.files
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('sort', models.IntegerField(default=100, help_text='\u0412\u043e\u0437\u043c\u043e\u0436\u043d\u044b \u043e\u0442\u0440\u0438\u0446\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u044f', verbose_name='#\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438', blank=True)),
                ('left', models.IntegerField(default=0, null=True, blank=True)),
                ('right', models.IntegerField(default=0, null=True, blank=True)),
                ('level', models.IntegerField(default=0, null=True, blank=True)),
                ('home', models.BooleanField(default=False, verbose_name='\u041d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439')),
                ('csv_adimport', models.CharField(max_length=255, null=True, verbose_name='CSV URL')),
                ('update_img', models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u0441\u0442\u043e\u0438\u0442 \u0433\u0430\u043b\u043e\u0447\u043a\u0430, \u0437\u043d\u0430\u0447\u0438\u0442 \u043f\u0440\u0438 \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u0438 \u0442\u043e\u0432\u0430\u0440\u043e\u0432 \u043e\u0431\u043d\u043e\u0432\u0438\u0442\u044c \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', verbose_name='Upd img')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0427\u041f\u0423 URL', blank=True)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('images', models.ImageField(upload_to=kernel.files.get_file_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('h1', models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b H1', blank=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='title', blank=True)),
                ('keywords', models.CharField(max_length=255, null=True, verbose_name='keywords', blank=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='description', blank=True)),
                ('text', tinymce.models.HTMLField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('last_import', models.DateTimeField(null=True, verbose_name='last import', blank=True)),
                ('parent', models.ForeignKey(related_name='category_parent', blank=True, to='catalog.Category', help_text='\u041f\u0443\u0441\u0442\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435, \u0435\u0441\u043b\u0438 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u0443\u0440\u043e\u0432\u043d\u044f', null=True, verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u0441\u043a\u0430\u044f \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f')),
            ],
            options={
                'ordering': ('-sort', 'name'),
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
                'verbose_name_plural': '01. \u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=False, db_index=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0427\u041f\u0423 URL', blank=True)),
                ('subtext', models.TextField(help_text='\u0421\u0438\u043d\u043e\u043d\u0438\u043c\u0438\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442', null=True, verbose_name='\u0414\u043e\u043f \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('price', models.IntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430', blank=True)),
                ('oldprice', models.IntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0440\u0430\u044f \u0426\u0435\u043d\u0430', blank=True)),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('id_adimport', models.CharField(max_length=255, unique=True, null=True, verbose_name='id_adimport', blank=True)),
                ('picture', models.TextField(null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True)),
                ('available', models.CharField(help_text='true: \u0412 \u043d\u0430\u043b\u0438\u0447\u0438\u0438, false: \u041f\u043e\u0434 \u0437\u0430\u043a\u0430\u0437', max_length=255, null=True, verbose_name='\u041d\u0430\u043b\u0438\u0447\u0438\u0435/\u0417\u0430\u043a\u0430\u0437', blank=True)),
                ('currency', models.CharField(max_length=255, null=True, verbose_name='\u0412\u0430\u043b\u044e\u0442\u0430', blank=True)),
                ('delivery', models.CharField(help_text='true: \u0415\u0441\u0442\u044c, false: \u041d\u0435\u0442', max_length=255, null=True, verbose_name='\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430', blank=True)),
                ('admitad_id', models.CharField(max_length=255, null=True, verbose_name='ID \u0432 \u0430\u0434\u043c\u0438\u0442\u0430\u0434\u0435', blank=True)),
                ('model', models.CharField(max_length=255, null=True, verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c', blank=True)),
                ('ref_url', models.CharField(max_length=255, null=True, verbose_name='\u0420\u0435\u0444\u0435\u0440\u0430\u043b\u043a\u0430', blank=True)),
                ('article', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0440\u0442\u0438\u043a\u0443\u043b \u0442\u043e\u0432\u0430\u0440\u0430', blank=True)),
                ('vendor_name', models.CharField(db_index=True, max_length=255, null=True, verbose_name='\u0411\u0440\u0435\u043d\u0434', blank=True)),
                ('advcampaign_id', models.CharField(db_index=True, max_length=255, null=True, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u043e\u0444\u0444\u0435\u0440\u0430 \u0443 admitad.com', blank=True)),
                ('advcampaign_name', models.CharField(db_index=True, max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043e\u0444\u0444\u0435\u0440\u0430 \u0443 admitad.com', blank=True)),
                ('param', models.TextField(null=True, verbose_name='Param', blank=True)),
                ('new_item', models.BooleanField(default=True, verbose_name='\u041d\u043e\u0432\u044b\u0439', editable=False)),
                ('delete', models.DateTimeField(verbose_name='\u0423\u0434\u0430\u043b\u0451\u043d', null=True, editable=False, blank=True)),
                ('category', models.ForeignKey(related_name='item_category', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category')),
            ],
            options={
                'ordering': ['create'],
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440\u044b',
                'verbose_name_plural': '02. \u0422\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='ItemOffer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('name', models.CharField(max_length=255, unique=True, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('code', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u043e\u0444\u0435\u0440\u0430', blank=True)),
                ('images', models.ImageField(upload_to=kernel.files.get_file_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043e\u0444\u0435\u0440\u0430', blank=True)),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0427\u041f\u0423 URL', blank=True)),
                ('min_text', tinymce.models.HTMLField(null=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442', blank=True)),
                ('max_text', tinymce.models.HTMLField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e-\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043e\u043a\u043d\u0430', blank=True)),
                ('keywords', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e-\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435', blank=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0415\u041e-\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('delivery', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430')),
                ('payment', tinymce.models.HTMLField(null=True, verbose_name='\u0421\u043f\u043e\u0441\u043e\u0431\u044b \u043e\u043f\u043b\u0430\u0442\u044b', blank=True)),
                ('return_policy', tinymce.models.HTMLField(null=True, verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u0435 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', blank=True)),
                ('guarantee', tinymce.models.HTMLField(null=True, verbose_name='\u0413\u0430\u0440\u0430\u043d\u0442\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u041e\u0444\u0435\u0440\u044b',
                'verbose_name_plural': '03. \u041e\u0444\u0435\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='ItemParams',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=20000, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('sub_value', models.CharField(help_text='\u0412\u0435\u043b\u0438\u0447\u0438\u043d\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f: \u041a\u0413, \u0421\u041c', max_length=500, null=True, verbose_name='Unit \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438',
                'verbose_name_plural': '05. \u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='ItemParamsName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='\u0426\u0432\u0435\u0442, \u0440\u0430\u0437\u043c\u0435\u0440, \u0432\u0435\u0441, ...', unique=True, max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0445\u0430\u0440-\u0442\u0438\u043a\u0438')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': '\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u044f \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a',
                'verbose_name_plural': '06. \u041d\u0430\u0437\u0432\u0430\u043d\u0438\u044f \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a',
            },
        ),
        migrations.CreateModel(
            name='ItemVendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('name', models.CharField(max_length=255, unique=True, null=True, verbose_name='\u0411\u0440\u0435\u043d\u0434', blank=True)),
                ('code', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u0431\u0440\u0435\u043d\u0434\u0430', blank=True)),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0427\u041f\u0423 URL', blank=True)),
            ],
            options={
                'verbose_name': '\u0411\u0440\u0435\u043d\u0434\u044b',
                'verbose_name_plural': '04. \u0411\u0440\u0435\u043d\u0434\u044b',
            },
        ),
        migrations.CreateModel(
            name='SemanticReviews',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(help_text='\u0420\u0430\u0437\u0434\u0435\u043b\u0438\u0442\u0435\u043b\u044c \u0441 \u043d\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u043e\u043a\u0438.', null=True, verbose_name='\u0421\u043f\u0438\u0441\u043e\u043a \u043e\u0442\u0437\u044b\u0432\u043e\u0432', blank=True)),
                ('category', models.OneToOneField(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category')),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432',
                'verbose_name_plural': '08. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='SemanticText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(help_text='\u0420\u0430\u0437\u0434\u0435\u043b\u0438\u0442\u0435\u043b\u044c \u0441 \u043d\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u043e\u043a\u0438.<br>', null=True, verbose_name='\u0421\u043f\u0438\u0441\u043e\u043a \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439', blank=True)),
                ('category', models.OneToOneField(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category')),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439',
                'verbose_name_plural': '07. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439',
            },
        ),
        migrations.CreateModel(
            name='SemanticTitle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(help_text='\u0420\u0430\u0437\u0434\u0435\u043b\u0438\u0442\u0435\u043b\u044c \u0441 \u043d\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u043e\u043a\u0438.<br>__names__ - \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043e\u0431\u044a\u0435\u043a\u0442\u0430(\u043d\u0435 \u043a\u0440\u0430\u0442\u043a\u043e\u0435),<br>__prices__ - \u0446\u0435\u043d\u0430 \u0442\u043e\u0432\u0430\u0440\u0430,<br>__offers__ - \u043e\u0444\u0444\u0435\u0440 \u0442\u043e\u0432\u0430\u0440\u0430,<br>__categorys__ - \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0442\u043e\u0432\u0430\u0440\u0430', null=True, verbose_name='\u0421\u043f\u0438\u0441\u043e\u043a \u043e\u0442\u0437\u044b\u0432\u043e\u0432', blank=True)),
                ('category', models.OneToOneField(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category')),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title',
                'verbose_name_plural': '09. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title',
            },
        ),
        migrations.AddField(
            model_name='itemparams',
            name='attr',
            field=models.ForeignKey(related_name='param_name', verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438', to='catalog.ItemParamsName'),
        ),
        migrations.AddField(
            model_name='itemparams',
            name='item',
            field=models.ForeignKey(related_name='param_item', verbose_name='\u0422\u043e\u0432\u0430\u0440', to='catalog.Item'),
        ),
        migrations.AddField(
            model_name='item',
            name='offer',
            field=models.ForeignKey(related_name='offer_item', verbose_name='Offer', blank=True, to='catalog.ItemOffer', null=True),
        ),
        migrations.AddField(
            model_name='item',
            name='vendor',
            field=models.ForeignKey(related_name='vendor_item', verbose_name='\u0411\u0440\u0435\u043d\u0434', blank=True, to='catalog.ItemVendor', null=True),
        ),
    ]

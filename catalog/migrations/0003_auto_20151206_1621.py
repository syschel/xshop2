# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20151202_2319'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filtersetting',
            options={'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432', 'verbose_name_plural': '07. \u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='semanticreviews',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432', 'verbose_name_plural': '09. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='semantictext',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439', 'verbose_name_plural': '08. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439'},
        ),
        migrations.AlterModelOptions(
            name='semantictitle',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title', 'verbose_name_plural': '10. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title'},
        ),
        migrations.AddField(
            model_name='item',
            name='title',
            field=models.CharField(help_text='\u0417\u0430\u043f\u043e\u043b\u043d\u044f\u0435\u0442\u0441\u044f \u0440\u043e\u0431\u043e\u0442\u043e\u043c \u043f\u043e \u0441\u043f\u0438\u0441\u043a\u0443 \u0448\u0430\u0431\u043b\u043e\u043d\u043e\u0432', max_length=255, null=True, verbose_name='SEO Title', blank=True),
        ),
    ]

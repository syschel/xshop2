# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='\u0412\u044b\u0432\u043e\u0434\u0438\u0442\u0441\u044f \u0432 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', max_length=300, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u043c\u043e\u0435 \u0438\u043c\u044f')),
                ('name_show', models.CharField(help_text='\u0412\u044b\u0432\u043e\u0434\u0438\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435', max_length=300, verbose_name='\u0418\u043c\u044f \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c')),
                ('categoryes', models.ManyToManyField(help_text='\u0421\u043f\u0438\u0441\u043e\u043a \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439 \u0433\u0434\u0435 \u043c\u043e\u0436\u043d\u043e \u0432\u044b\u0432\u0435\u0441\u0442\u0438 \u044d\u0442\u043e\u0442 \u0444\u0438\u043b\u044c\u0442\u0440', to='catalog.Category', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f')),
                ('param_name', models.ForeignKey(verbose_name='\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0430 \u0438\u0437 CSV', to='catalog.ItemParamsName')),
            ],
            options={
                'verbose_name': '\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432',
                'verbose_name_plural': '08. \u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='FilterSettingValues',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=300, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('param_name', models.ForeignKey(related_name='filters_settings', verbose_name='\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u0430', to='catalog.FilterSetting')),
            ],
            options={
                'verbose_name': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.AlterModelOptions(
            name='semanticreviews',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432', 'verbose_name_plural': '08. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u0442\u0437\u044b\u0432\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='semantictext',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439', 'verbose_name_plural': '07. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0439'},
        ),
        migrations.AlterModelOptions(
            name='semantictitle',
            options={'ordering': ['category_id'], 'verbose_name': '\u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title', 'verbose_name_plural': '09. \u0421\u043f\u0438\u0441\u043e\u043a\u0438 Title'},
        ),
    ]

#-*- coding:utf-8 -*-
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic import DetailView, ListView, View
from django.shortcuts import get_object_or_404
import random
from catalog.tools import gen_page_list, get_random_string
from catalog.models import Category, Item
from content.views import pages_index
from statistic.models import SubId


class MainView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.filter(home=True, show=True).\
            only('pk', 'sort', 'url', 'name', 'images')
        # context['items'] = Item.objects.filter(show=True, picture__isnull=False).\
        #     only('pk', 'url', 'name', 'price', 'oldprice', 'currency', 'picture')[:12]
        context['slid_pages'] = pages_index
        return context


class CategoryListView(ListView):
    template_name = 'catalog/category_list.html'
    paginate_by = 99

    def get_category(self):
        return get_object_or_404(Category, pk=self.kwargs.get("pk"))  # , url=self.kwargs.get("slug")

    def get_breadcrumb(self):
        return self.get_category()

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['category'] = self.get_category()
        page_number = self.kwargs.get(self.page_kwarg) or self.request.GET.get(self.page_kwarg) or 1
        page_count = context['paginator'].num_pages
        context['page_lists'] = gen_page_list(int(page_number), int(page_count))
        return context

    def get_queryset(self):
        category = self.get_category()
        categories = Category.objects.filter(left__gte=category.left, right__lte=category.right).values_list('pk', flat=True)
        items = Item.objects.filter(category__in=categories).only('pk', 'url', 'name', 'price', 'oldprice', 'currency', 'picture')
        return items


class ItemDetailView(DetailView):
    model = Item
    slug_field = 'url'
    query_pk_and_slug = False

    def get_similars(self):
        similar0 = Item.objects.filter(category=self.object.category, show=True, pk__lt=self.object.pk).\
                       values_list("pk", flat=True).order_by()[:20]
        similar1 = Item.objects.filter(category=self.object.category, show=True, pk__gt=self.object.pk).\
                       values_list("pk", flat=True).order_by()[:20]
        similar2 = list(similar0) + list(similar1)
        similar_count = len(similar2)
        item_id = []
        if similar_count > 2:
            while len(item_id) < 4:
                item_id.append(similar2[random.randint(0, similar_count-1)])
        similars = Item.objects.filter(pk__in=item_id).only('pk', 'url', 'name', 'price', 'oldprice', 'currency', 'picture')
        return similars

    def get_context_data(self, **kwargs):
        context = super(ItemDetailView, self).get_context_data(**kwargs)
        context['category'] = context['object'].category
        context['similars'] = self.get_similars()
        return context


class ItemGoRedirectView(RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        item_id = kwargs['pk']
        item = get_object_or_404(Item, pk=item_id)
        ip = self.request.META.get('REMOTE_ADDR', '') or self.request.META.get('HTTP_X_FORWARDED_FOR', '')

        url = item.ref_url
        subid_num = u"%s_%s" % (item_id, get_random_string(12))
        pars_url = url.split("?")
        subid = SubId()
        subid.subid = subid_num
        subid.ip = ip
        subid.keywords = ""
        subid.item_id = item_id
        subid.item_name = item.name
        subid.item_url = item.get_absolute_url()
        if "LANKA_REF" in self.request.COOKIES:
            subid.referer = self.request.COOKIES.get('LANKA_REF', False).decode("utf-8")
            subid.status = SubId.TEMPL0
        else:
            subid.status = SubId.TEMPL1

        if len(pars_url) == 2:
            url = u"%s?subid=%s&%s" % (pars_url[0], subid_num, pars_url[1])
        else:
            subid.status = SubId.TEMPL2
        subid.save()

        return url

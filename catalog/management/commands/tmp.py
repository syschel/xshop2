# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Trees categores left/right'

    # def handle(self, *args, **options):
        # a = 'a:50:{i:0;s:11:"id_adimport";i:1;s:4:"ISBN";i:2;s:5:"adult";i:3;s:3:"age";i:4;s:7:"article";i:5;s:5:"attrs";i:6;s:6:"author";i:7;s:9:"available";i:8;s:7:"barcode";i:9;s:7:"binding";i:10;s:5:"brand";i:11;s:10:"categoryId";i:12;s:17:"country_of_origin";i:13;s:10:"currencyId";i:14;s:8:"delivery";i:15;s:11:"description";i:16;s:12:"downloadable";i:17;s:6:"format";i:18;s:6:"gender";i:19;s:2:"id";i:20;s:19:"local_delivery_cost";i:21;s:21:"manufacturer_warranty";i:22;s:15:"market_category";i:23;s:5:"model";i:24;s:13:"modified_time";i:25;s:4:"name";i:26;s:8:"oldprice";i:27;s:12:"orderingTime";i:28;s:11:"page_extent";i:29;s:5:"param";i:30;s:12:"performed_by";i:31;s:6:"pickup";i:32;s:7:"picture";i:33;s:5:"price";i:34;s:9:"publisher";i:35;s:11:"sales_notes";i:36;s:6:"series";i:37;s:5:"store";i:38;s:4:"syns";i:39;s:9:"topseller";i:40;s:4:"type";i:41;s:10:"typePrefix";i:42;s:3:"url";i:43;s:6:"vendor";i:44;s:10:"vendorCode";i:45;s:6:"weight";i:46;s:4:"year";i:47;s:14:"advcampaign_id";i:48;s:16:"advcampaign_name";i:49;s:8:"deeplink";}'
        # a = 'a:9:{' \
        #         's:8:"Цвет";a:1:{s:32:"коричневый, серый";s:0:"";}' \
        #         's:18:"Коллекция";a:1:{s:29:"Осень-зима 2014/2015";s:0:"";}' \
        #         's:31:"Внешний материал";a:1:{s:33:"натуральная замша";s:0:"";}' \
        #         's:20:"Сезонность";a:1:{s:28:"Демисезон, Зима";s:0:"";}' \
        #         's:31:"Материал подошвы";a:1:{s:12:"резина";s:0:"";}' \
        #         's:37:"Страна-изготовитель";a:1:{s:10:"Китай";s:0:"";}' \
        #         's:7:"Unit=RU";a:4:{i:36;s:0:"";i:39;s:0:"";i:40;s:0:"";i:38;s:0:"";}' \
        #         's:6:"Пол";a:1:{s:14:"Женский";s:0:"";}' \
        #         's:14:"Возраст";a:1:{s:16:"Взрослый";s:0:"";}' \
        #     '}'
        # import phpserialize
        # print phpserialize.loads(a, array_hook=OrderedDict)
        # c = dict(phpserialize.loads(a))
        # for key, value in c.items():
        #     print key, ':'
        #     for aa in value:
        #         print aa
        # print list(OrderedDict(phpserialize.loads(a, array_hook=OrderedDict)))

    def handle(self, *args, **options):
        a = u"'id_adimport'|'ISBN'|'adult'|'age'|'article'|'attrs'|'author'|'available'|'barcode'|'binding'|'brand'|'categoryId'|'country_of_origin'|'currencyId'|'delivery'|'description'|'downloadable'|'format'|'gender'|'id'|'local_delivery_cost'|'manufacturer_warranty'|'market_category'|'model'|'modified_time'|'name'|'oldprice'|'orderingTime'|'page_extent'|'param'|'performed_by'|'pickup'|'picture'|'price'|'publisher'|'sales_notes'|'series'|'store'|'syns'|'topseller'|'type'|'typePrefix'|'url'|'vendor'|'vendorCode'|'weight'|'year'|'advcampaign_id'|'advcampaign_name'|'deeplink'"
        aa = a.replace("'", "").split("|")

        create_table = "CREATE TABLE IF NOT EXISTS `adimport_tmp` (" \
                       "`id_adimport` int(11) unsigned NOT NULL AUTO_INCREMENT,"
        for value in aa[1:]:
            create_table += "`%s` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL," % value
        create_table += " PRIMARY KEY (`id_adimport`), KEY `modified_time` (`modified_time`))" \
                        " ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;"
        from django.db import connection
        cursor = connection.cursor()
        print "0. create_table:", cursor.execute(create_table)
        from django.conf import settings
        file_url = "%s/files/adimport_items.csv" % settings.BASE_DIR
        load_data = "LOAD DATA LOCAL INFILE '%s' INTO TABLE adimport_tmp CHARACTER SET utf8 FIELDS TERMINATED BY '|' ENCLOSED BY \"'\" LINES TERMINATED BY '\\n' (%s);" % (file_url, ','.join(aa))

        print "1. change varchar to text:", cursor.execute("ALTER TABLE `adimport_tmp` CHANGE `name` `name` TEXT, CHANGE `description` `description` TEXT, CHANGE `param` `param` TEXT, CHANGE `picture` `picture` TEXT, CHANGE `url` `url` TEXT;")
        print "2. load_data:", cursor.execute(load_data)
        print "3. delete 0 line file:", cursor.execute("DELETE FROM `adimport_tmp` WHERE `modified_time`='modified_time'")
        print "4. remove fields:", cursor.execute("ALTER TABLE `adimport_tmp`DROP `ISBN`,DROP `adult`,DROP `age`,DROP `attrs`,DROP `author`,DROP `barcode`,DROP `binding`,DROP `brand`,DROP `categoryId`,DROP `country_of_origin`,DROP `downloadable`,DROP `format`,DROP `gender`,DROP `local_delivery_cost`,DROP `manufacturer_warranty`,DROP `market_category`,DROP `model`,DROP `modified_time`,DROP `orderingTime`,DROP `page_extent`,DROP `performed_by`,DROP `pickup`,DROP `publisher`,DROP `sales_notes`,DROP `series`,DROP `store`,DROP `syns`,DROP `topseller`,DROP `type`,DROP `typePrefix`,DROP `vendorCode`,DROP `weight`,DROP `year`,DROP `deeplink`;")
        print

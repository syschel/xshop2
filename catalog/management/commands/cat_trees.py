# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count
from kernel.robot import catalog_trees


class Command(BaseCommand):
    help = 'Trees categores left/right'

    def handle(self, *args, **options):
        print catalog_trees()
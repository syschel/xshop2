# -*- coding: utf-8 -*-
import os, re
from django import template
from catalog.models import Category
register = template.Library()


@register.inclusion_tag('tags/catalog_top_menu.html', takes_context=True)
def catalog_top_menu(context):
    # Верхнее сквозное меню товаров
    categories = Category.objects.filter(show=True, level__in=[1, 2])\
        .only('pk', 'parent', 'level', 'sort', 'url', 'name').\
        order_by('level', 'sort')
    return {'categories': categories}


@register.filter(name='ru_price')
def ru_price(value, decimal_points=3, seperator=u' '):  # Only one argument.
    value = str(value)
    if len(value) <= decimal_points:
        return value
    # say here we have value = '12345' and the default params above
    parts = []
    while value:
        parts.append(value[-decimal_points:])
        value = value[:-decimal_points]
    # now we should have parts = ['345', '12']
    parts.reverse()
    # and the return value should be u'12.345'
    return seperator.join(parts)


@register.filter(name='img_prew')
def img_prew(value):
    values = value.split(' ')
    return values[0] if values else value


@register.filter(name='split')
def slpit_filter(value, arg):
    return value.split(arg)
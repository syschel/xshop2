# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import TextInput
from catalog.models import Category, Item, ItemOffer, ItemVendor, ItemParamsName, ItemParams,\
    SemanticText, SemanticReviews, SemanticTitle, FilterSetting, FilterSettingValues


class ModelAdmin(admin.ModelAdmin):
    formfield_overrides = {models.CharField: {'widget': TextInput(attrs={'size': '94'})}}


class CategoryAdmin(ModelAdmin):
    list_display = ('id', 'name', 'parent', 'level', 'home', 'left', 'right', 'sort', 'show', 'create', 'update', 'update_img', 'last_import')
    list_display_links = ('id', 'name')
    list_filter = ('show', 'home', 'level',)
    search_fields = ['name', 'id']
    list_editable = ('sort', 'home', 'show', 'update_img')
    raw_id_fields = ('parent',)
    readonly_fields = ('left', 'right')
    save_on_top = True

    fieldsets = (
        (None, {'fields': ('csv_adimport', 'update_img', 'last_import')}),
        (u"Общее", {'fields': ('name', 'h1', 'text', 'images')}),
        (u"Настройки", {'fields': ('parent', 'level', 'left', 'right', 'sort', 'show', 'home', 'url')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )


class ItemAdmin(ModelAdmin):
    list_display = ('name', 'category_id_display', 'price', 'oldprice', 'new_item', 'show', 'create', 'update')
    list_filter = ('show', 'new_item')

    def category_id_display(self, obj):
        return obj.category_id
    category_id_display.short_description = 'Category ID'


class ItemOfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'code', 'delivery', 'show', 'create', 'update')
    list_display_links = ('name', 'code')
    list_filter = ('show',)
    search_fields = ['name']
    ordering = ('-name',)
    save_on_top = True

    fieldsets = (
        (u"Настройки", {'fields': ('name', 'url', 'code', 'show', 'images')}),
        (u"Общее", {'fields': ('min_text', 'max_text', 'delivery', 'payment', 'return_policy', 'guarantee')}),
        (u"SEO", {'fields': ('title', 'keywords', 'description')}),
    )


class ItemVendorAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'show', 'create', 'update')
    list_display_links = ('name', 'code')
    list_filter = ('show',)
    search_fields = ['name']
    ordering = ('name',)


class ItemParamsAdmin(admin.ModelAdmin):
    list_display = ('attr_id', 'value', 'sub_value')
    list_display_links = ('attr_id', 'value',)
    list_filter = ('attr',)
    raw_id_fields = ("item",)

    def attr_id(self, instance):
        return instance.attr.pk


class ItemParamsNameAdmin(admin.ModelAdmin):
    list_display = ('name', 'show')
    list_display_links = ('name',)
    list_filter = ('show',)
    ordering = ('name',)


class FilterSettingValuesInline(admin.TabularInline):
    model = FilterSettingValues
    extra = 1


class FilterSettingAdmin(admin.ModelAdmin):
    list_display = ('name_show', 'name', 'show',)
    list_display_links = ('name_show', 'name',)
    list_filter = ('show',)
    filter_horizontal = ('categoryes',)
    raw_id_fields = ('categoryes',)

    def cat_name(self, instance):
        return instance.categoryes.pk

    inlines = [FilterSettingValuesInline]


class SemanticAdmin(admin.ModelAdmin):
    list_display = ('category_id_display', 'category')
    list_display_links = ('category_id_display', 'category')
    raw_id_fields = ('category',)

    def category_id_display(self, obj):
        return obj.category_id

admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemOffer, ItemOfferAdmin)
admin.site.register(ItemVendor, ItemVendorAdmin)
admin.site.register(ItemParams, ItemParamsAdmin)
admin.site.register(ItemParamsName, ItemParamsNameAdmin)
admin.site.register(FilterSetting, FilterSettingAdmin)
admin.site.register(SemanticText, SemanticAdmin)
admin.site.register(SemanticReviews, SemanticAdmin)
admin.site.register(SemanticTitle, SemanticAdmin)

# -*- coding: utf-8 -*-
from catalog.models import Item
from content.models import BaseSettings, FooterMenu


def site(request):
    site = BaseSettings.objects.filter(template_seo=0)
    return {
        'base_item': Item.objects.all().count(),
        'site': site[0] if site else None,
        'footer_menu': FooterMenu.objects.all()
    }
# -*- coding: utf-8 -*-


def catalog_trees():
    """
    Перестроение дерева в категориях каталога
    """

    from catalog.models import Category
    Category.objects.all().update(left=None, right=None)
    cat_null = Category.objects.filter(parent__isnull=True)

    left = 0
    for catnu in cat_null:
        left += 1
        catnu.left = left
        catnu.level = 1
        if catnu.category_parent.all():
            for catn2 in catnu.category_parent.all():
                left += 1
                catn2.left = left
                catn2.level = 2
                if catn2.category_parent.all():
                    for catn3 in catn2.category_parent.all():
                        left += 1
                        catn3.left = left
                        catn3.level = 3
                        if catn3.category_parent.all():
                            for catn4 in catn3.category_parent.all():
                                left += 1
                                catn4.left = left
                                catn4.level = 4
                                if catn4.category_parent.all():
                                    for catn5 in catn4.category_parent.all():
                                        left += 1
                                        catn5.left = left
                                        catn5.level = 5
                                        if catn5.category_parent.all():
                                            for catn6 in catn5.category_parent.all():
                                                left += 1
                                                catn6.left = left
                                                left += 1
                                                catn6.right = left
                                                catn6.level = 6
                                                catn6.save()
                                        left += 1
                                        catn5.right = left
                                        catn5.save()
                                left += 1
                                catn4.right = left
                                catn4.save()
                        left += 1
                        catn3.right = left
                        catn3.save()
                left += 1
                catn2.right = left
                catn2.save()
        left += 1
        catnu.right = left
        catnu.save()

    return True
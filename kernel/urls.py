#-*- coding:utf-8 -*-
"""cms_cpa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from catalog.views import MainView, CategoryListView, ItemDetailView, ItemGoRedirectView
from content.views import Robotstxt

urlpatterns = [
    url(r'^cat_(?P<slug>\w+)_(?P<pk>\d+)/$', CategoryListView.as_view(), name='category'),
    url(r'^item_(?P<slug>\w+)_(?P<pk>\d+)/$', ItemDetailView.as_view(), name='item'),
    url(r'^item_go/(?P<pk>\d+)/$', ItemGoRedirectView.as_view(), name='redirect_item'),
    url(r'^robots.txt$', Robotstxt.as_view(), name='robotstxt'),

    url(r'^page/', include('content.urls', namespace='content')),
    url(r'^$', MainView.as_view(), name='home'),

    url(r'^admin/', include(admin.site.urls)),
]

# Инициализируем папку media
if settings.DEBUG:
    if settings.MEDIA_ROOT:
        from django.conf.urls.static import static
        from django.contrib.staticfiles.urls import staticfiles_urlpatterns
        urlpatterns += static(settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)
    # Эта строка опциональна и будет добавлять url'ы только при DEBUG = True
    urlpatterns += staticfiles_urlpatterns()
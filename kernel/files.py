# -*- coding: utf-8 -*-
import os
import uuid
import hashlib


def get_file_path(instance, filename):
    """
    Изменяем имя файла и уникализируем путь до него
    :param instance: Объект из которого была запущена функция
    :param filename: Текущее имя файла. Нужно только для расширения
    :return: Имя файла с путём до директории
    """
    # Переименовываем файл
    ext = filename.split('.')[-1]
    # Генерируем рандомный md5(md5+md5)
    md5 = hashlib.md5(uuid.uuid4().get_hex().encode() + uuid.uuid4().get_hex().encode()).hexdigest()

    filename = u"%s.%s" % (md5[:-4], ext)
    # Создаём подпапки из начала имени файла
    path = u'/%s/%s/' % (md5[:2], md5[2:4])

    # Определяем от какого класса у нас загрузка файла
    name = instance.__class__.__name__

    return os.path.join("upload/md/" + name + path, filename)
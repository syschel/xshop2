# -*- coding: utf-8 -*-
from django.db import models


class SubId(models.Model):
    """
    SUBid статистика
    """

    create = models.DateTimeField(verbose_name=u"Создано", auto_now_add=True)
    subid = models.CharField(verbose_name=u'SubID', max_length=255, null=True, blank=False)
    referer = models.TextField(verbose_name=u'Рефка', null=True, blank=False)
    keywords = models.TextField(verbose_name=u'Слова в запрсе', null=True, blank=False)
    item_id = models.CharField(verbose_name=u'ИД товара', max_length=255, null=True, blank=False)
    item_name = models.CharField(verbose_name=u'Название товара', max_length=500, null=True, blank=False)
    item_url = models.CharField(verbose_name=u'Ссылка на товар', max_length=500, null=True, blank=False)
    ip = models.GenericIPAddressField(verbose_name=u'IP', null=True, blank=False, protocol='IPv4')

    TEMPL0, TEMPL1, TEMPL2 = range(0, 3)
    TEMPLATE = (
        (TEMPL0, u'Внешний'),
        (TEMPL1, u'Прямой'),
        (TEMPL2, u'Ошибка'),
    )
    status = models.IntegerField(verbose_name=u'#Заход', default=TEMPL0, choices=TEMPLATE)

    def __unicode__(self):
        return u"%s" % self.subid

    class Meta:
        verbose_name = u'SubID'
        verbose_name_plural = u'SubID'
        ordering = ('-create',)
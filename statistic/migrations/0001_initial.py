# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SubId',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('subid', models.CharField(max_length=255, null=True, verbose_name='SubID')),
                ('referer', models.TextField(null=True, verbose_name='\u0420\u0435\u0444\u043a\u0430')),
                ('keywords', models.TextField(null=True, verbose_name='\u0421\u043b\u043e\u0432\u0430 \u0432 \u0437\u0430\u043f\u0440\u0441\u0435')),
                ('item_id', models.CharField(max_length=255, null=True, verbose_name='\u0418\u0414 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('item_name', models.CharField(max_length=500, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('item_url', models.CharField(max_length=500, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0442\u043e\u0432\u0430\u0440')),
                ('ip', models.GenericIPAddressField(null=True, verbose_name='IP', protocol=b'IPv4')),
                ('status', models.IntegerField(default=0, verbose_name='#\u0417\u0430\u0445\u043e\u0434', choices=[(0, '\u0412\u043d\u0435\u0448\u043d\u0438\u0439'), (1, '\u041f\u0440\u044f\u043c\u043e\u0439'), (2, '\u041e\u0448\u0438\u0431\u043a\u0430')])),
            ],
            options={
                'ordering': ('-create',),
                'verbose_name': 'SubID',
                'verbose_name_plural': 'SubID',
            },
        ),
    ]
